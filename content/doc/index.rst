Documentation
=============

Screencast
----------

.. raw:: html

   A brief video introduction of Kwant:
   <a href="kwant-screencast-2014.html">watch</a>,
   <a href="http://downloads.kwant-project.org/doc/kwant-screencast-2014.mp4" download>download</a>.

Tutorial and reference manual
-----------------------------

* Browse online: `stable version </doc/1/>`_, `development version </doc/dev/>`_,
* `download PDF <http://downloads.kwant-project.org/doc/kwant-doc-1.1.1.pdf>`_,
* `download zipped HTML
  <http://downloads.kwant-project.org/doc/kwant-doc-1.1.1.zip>`_ (for reading
  without web access).

Interactive online course
-------------------------
The APS March meeting 2016 tutorial “Introduction to Computational Quantum Nanoelectronics” focuses on the physics, but also serves as a crash course on Kwant.  `All the materials are available online </mm16.html>`_ and can be run directly in a web browser, without installing Kwant locally.

Article
-------

This paper (`download PDF <http://downloads.kwant-project.org/doc/kwant-paper.pdf>`__) introduces Kwant in a systematic way and discusses its design and performance (`New J. Phys. 16, 063065 (2014) <http://iopscience.iop.org/1367-2630/16/6/063065/article>`_).
