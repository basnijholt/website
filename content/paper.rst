Kwant reference
---------------

C. W. Groth, M. Wimmer, A. R. Akhmerov, X. Waintal,
*Kwant: a software package for quantum transport*,
`New J. Phys. 16, 063065 (2014)
<http://iopscience.iop.org/1367-2630/16/6/063065/article>`_.
