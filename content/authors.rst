================
Authors of Kwant
================

The principal developers of kwant are:

* `Christoph W. Groth <mailto:christoph.groth@cea.fr>`_ (SPSMS-INAC-CEA
  Grenoble)
* `Michael Wimmer <http://www.lorentz.leidenuniv.nl/~wimmer/>`_ (TU Delft)
* `Anton R. Akhmerov <http://antonakhmerov.org>`_ (TU Delft)
* `Xavier Waintal <http://inac.cea.fr/Pisp/xavier.waintal/>`_ (SPSMS-INAC-CEA
  Grenoble)

Kwant authors should be reached via email authors@kwant-project.org. Please use
the `mailing list </community.html>`_ for any Kwant-related question that can
be discussed publicly.

Other people that have contributed to Kwant include

* Daniel Jaschke (SPSMS-INAC-CEA Grenoble)
* Joseph Weston (SPSMS-INAC-CEA Grenoble)

We thank Christoph Gohlke for the creation of Windows installers.

`CEA <http://cea.fr>`_ is the French Commissariat à l'énergie atomique et aux
énergies alternatives.  The CEA is the copyright holder for the contributions of
C. W. Groth, X. Waintal, and its other employees involved in Kwant.

To find out who wrote a certain part of Kwant, please use the "blame" feature of
`Git <http://git-scm.com/>`_, the version control system.


Funding
-------

During the development of Kwant 1.0, A. R. Akhmerov and M. Wimmer were supported
by the Dutch Science Foundation NWO/FOM and by the ERC Advanced Investigator
Grant of C. W. J. Beenakker who enthousiastically supported this project.
A. R. Akhmerov was partially supported by a Lawrence Golub fellowship.
C. W. Groth and X. Waintal were supported by the ERC Consolidator Grant MesoQMC.
X. Waintal also acknowledges support from the STREP ConceptGraphene.
