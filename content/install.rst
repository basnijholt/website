================================
Installing Kwant
================================

License and citation request
============================

Kwant is free software covered by the `2-clause BSD license </license>`_.

If you have used Kwant for work that has lead to a scientific publication,
please `cite the Kwant paper and possibly other relevant publications
</cite>`_.


Overview of available installation methods
==========================================

The quickest and easiest way to install Kwant is using the prepared packages
that are available for GNU/Linux (`Debian <#debian-and-derivatives>`_, `Ubuntu
<#ubuntu-and-derivatives>`_, and their variants, `Arch Linux`_), `Mac OS X`_,
and `Microsoft Windows`_.

Like most Python packages, `Kwant can be also installed using pip
<#automatic-installation-using-pip>`_.  (Be sure to follow this link, since
naive use of pip will likely result in a Kwant installation with significantly
reduced performance.)

If no packages are available for the system you use, or if you would like to
build Kwant from source for another reason (expert users may want to customize
Kwant to use certain optimized versions of libraries), please consult the
documentation on `how to install Kwant from source <doc/1/pre/install.html>`_.


Python 3 or Python 2
====================

Before installing Kwant, one has to decide which Python version to use –
Python 3 or Python 2.  (`Python 3.0
<https://docs.python.org/3/whatsnew/3.0.html>`_ was the first ever
intentionally backwards incompatible Python release, i.e. scripts that target
Python 2 will typically not run with Python 3 and above unchanged.)

Kwant releases up to 1.1 require Python 2.  Starting with release 1.2, Kwant
development has `switched to Python 3
<http://kwant-project.org/doc/1/pre/whatsnew/1.2>`_.  We recommend to use the
latest Kwant with Python 3.  Those who are stuck with Python 2 can continue to
use Kwant 1.1 which will be maintained for several years after 2015.

The instructions below assume Python 3.  They should be also valid for Python
2 if all occurrences of ``python3``, ``pip3``, etc. are replaced by
``python``, ``pip``.


Debian and derivatives
======================

The easiest way to install Kwant on a Debian system is using the pre-built
packages we provide.  Our packages are known to work with Debian "stable" and
Debian "testing", but they may also work on many other recent Debian-derived
systems as well.  (For example, the following works with recent Ubuntu
versions, too.)

The lines prefixed with ``sudo`` have to be run as root.

1. Add the following lines to ``/etc/apt/sources.list``::

       deb http://downloads.kwant-project.org/debian/ stable main
       deb-src http://downloads.kwant-project.org/debian/ stable main

2. (Optional) Add the OpenPGP key used to sign the repositories by executing::

       sudo apt-key adv --keyserver pool.sks-keyservers.net --recv-key C3F147F5980F3535

   The fingerprint of the key is 5229 9057 FAD7 9965 3C4F 088A C3F1 47F5 980F
   3535.

3. Update the package data, and install Kwant::

       sudo apt-get update
       sudo apt-get install python3-kwant python-kwant-doc

   The ``python-kwant-doc`` package is optional and installs the HTML
   documentation of Kwant in the directory ``/usr/share/doc/python-kwant-doc``.

Should the last command (``apt-get install``) fail due to unresolved
dependencies, you can try to build and install your own packages, which is
surprisingly easy::

    cd /tmp

    sudo apt-get build-dep tinyarray
    apt-get source --compile tinyarray
    sudo dpkg --install python3-tinyarray_*.deb

    sudo apt-get build-dep kwant
    apt-get source --compile kwant
    sudo dpkg --install python3-kwant_*.deb python-kwant-doc_*.deb

This method should work for all Debian-derived systems, even on exotic
architectures.


Ubuntu and derivatives
======================

Execute the following commands::

    sudo apt-add-repository ppa:kwant-project/ppa
    sudo apt-get update
    sudo apt-get install python3-kwant python-kwant-doc

This should provide Kwant for all versions of Ubuntu >= 12.04.  The HTML
documentation will be installed locally in the directory
``/usr/share/doc/python-kwant-doc``.


Arch Linux
==========

`Arch install scripts for Kwant
<https://aur.archlinux.org/packages/python-kwant/>`_ are kindly provided by
Jörg Behrmann (formerly by Max Schlemmer).  To install, follow the `Arch User
Repository installation instructions
<https://wiki.archlinux.org/index.php/Arch_User_Repository#Installing_packages>`_.
Note that for checking the validity of the package you need to add the key
used for signing to your user's keyring via::

    gpg --keyserver pool.sks-keyservers.net --recv-key C3F147F5980F3535

The fingerprint of the key is 5229 9057 FAD7 9965 3C4F 088A C3F1 47F5 980F
3535.


Mac OS X
========

There is a number of different package managers for bringing software from the
Unix/Linux world to Mac OS X. Since the community is quite split, we provide
Kwant and its dependencies both via the `homebrew <http://brew.sh>`_ and the
`MacPorts <http://www.macports.org>`_ systems.


Mac OS X: homebrew
==================

*(This section needs to be updated for Python 3.)*

homebrew is a recent addition to the package managers on Mac OS X. It is
lightweight, tries to be as minimalistic as possible and give the user
freedom than Macports. We recommend this option if you have no preferences.

1. Open a terminal and install homebrew as described on the `homebrew
   homepage <http://brew.sh>`_ (instructions are towards the end of
   the page)

2. Run ::

       brew doctor

   and follow its directions. It will ask for a few prerequisites to be
   installed, in particular

   * the Xcode developer tools (compiler suite for Mac OS X) from
     `<http://developer.apple.com/downloads>`_. You will need an Apple ID to
     download. Note that if you have one already from using the App store on the
     Mac/Ipad/Iphone/... you can use that one. Downloading the command line
     tools (not the full Xcode suite) is sufficient. If you have the full Xcode
     suite installed, you might need to download the command line tools manually
     if you have version 4 or higher. In this case go to `Xcode->Preferences`,
     click on `Download`, go to `Components`, select `Command Line Tools` and
     click on `Install`.
   * although `brew doctor` might not complain about it right away, while we're
     at it, you should also install the X11 server from the `XQuartz project
     <http://xquartz.macosforge.org>`_ if you have Mac OS X 10.8 or higher.

3. Add permanently ``/usr/local/bin`` before ``/usr/bin/`` in the ``$PATH$``
   environment variable of your shell, for example by adding ::

       export PATH=/usr/local/bin:$PATH

   at the end of your ``.bash_profile`` or ``.profile``. Then close
   the terminal and reopen it again.

4. Install a few prerequisites ::

       brew install gcc python

5. Add additional repositories ::

       brew tap homebrew/science
       brew tap homebrew/python
       brew tap kwant-project/kwant

6. Install Kwant and its prerequisites ::

       pip install nose six
       brew install numpy scipy matplotlib
       brew install kwant

Notes:

- If something does not work as expected, use ``brew doctor`` for
  instructions (it will find conflicts and things like that).
- As mentioned, homebrew allows for quite some freedom. In particular,
  if you are an expert, you don't need necessarily to install
  numpy/scipy/matplotlib from homebrew, but can use your own installation.
  The only prerequisite is that they are importable from python. (the
  Kwant installation will in any case complain if they are not)
- In principle, you need not install the homebrew python, but could use
  Apple's already installed python. Homebrew's python is more up-to-date,
  though.


Mac OS X: MacPorts
==================

*(This section needs to be updated for Python 3.)*

MacPorts is a full-fledged package manager that recreates a whole Linux-like
environment on your Mac.

In order to install Kwant using MacPorts, you have to

1. Install a recent version of MacPorts, as explained in the
   `installation instructions of MacPorts
   <http://www.macports.org/install.php>`_.
   In particular, as explained there, you will have to install also a
   few prerequisites, namely

   * the Xcode developer tools (compiler suite for Mac OS X) from
     `<http://developer.apple.com/downloads>`_. You will need an Apple ID to
     download. Note that if you have one already from using the App store
     on the Mac/Ipad/Iphone/... you can use that one. You will also need the
     command line tools: Within Xcode 4, you have to download them by going to
     `Xcode->Preferences`, click on `Download`, go to `Components`,
     select `Command Line Tools` and click on `Install`. Alternatively, you can
     also directly download the command line tools from the
     Apple developer website.
   * if you have Mac OS X 10.8 or higher, the X11 server from the
     `XQuartz project <http://xquartz.macosforge.org>`_.

2. After the installation, open a terminal and execute ::

       echo http://downloads.kwant-project.org/macports/ports.tar |\
       sudo tee -a /opt/local/etc/macports/sources.conf >/dev/null

   (this adds the Kwant MacPorts download link
   `<http://downloads.kwant-project.org/macports/ports.tar>`_ at the end of the
   ``sources.conf`` file.)

3. Execute ::

       sudo port selfupdate

4. Now, install Kwant and its prerequisites ::

       sudo port install py27-kwant

5. Finally, we choose python 2.7 to be the default python ::

       sudo port select --set python python27

   After that, you will need to close and reopen the terminal to
   have all changes in effect.

Notes:

* If you have problems with macports because your institution's firewall
  blocks macports (more precisely, the `rsync` port), resulting in
  errors from ``sudo port selfupdate``, follow
  `these instructions <https://trac.macports.org/wiki/howto/PortTreeTarball>`_.
* Of course, if you already have macports installed, you can skip step 1
  and continue with step 2.


Microsoft Windows
=================

There are multiple distributions of scientific Python software for Windows that
provide the prerequisites for Kwant.  We recommend to use the packages kindly
provided by Christoph Gohlke.  To install Kwant on Windows

1. Determine whether you have a 32-bit or 64-bit Windows installation by
   following these `instructions <http://support.microsoft.com/kb/827218>`_.

2. Download and install Python for the appropriate architecture (32-bit: “x86” or
   64-bit: “x86-64”) from the official `Python download site for Windows
   <http://www.python.org/download/>`_.  We recommend that you install the
   latest stable Python 3 release.

3. Open a command prompt, as described in "How do I get a command prompt" at
   the `Microsoft Windows website <http://windows.microsoft.com/en-us/windows/command-prompt-faq>`_.

4. In the command prompt window, execute::

        C:\Python35\python.exe C:\Python35\Tools\Scripts\win_add2path.py

   (Instead of typing this command, you can also just copy it from here and
   paste it into the command prompt window). If you did not use the default
   location to install Python in step 2, then replace ``C:\Python35`` by the
   actual location where Python is installed.  You may also need to adjust the
   version (“35” signifies Python 3.5).

5. Reboot your computer.

6. Download the necessary packages (with the ending ``.whl``) for your
   operating system (32 or 64 bit) and Python version (e.g. ``cp35`` for
   Python 3.5) from the website of `Christoph Gohlke
   <http://www.lfd.uci.edu/~gohlke/pythonlibs/>`_.  For Kwant, we recommend to
   download at least `NumPy
   <http://www.lfd.uci.edu/~gohlke/pythonlibs/#numpy>`_, `SciPy
   <http://www.lfd.uci.edu/~gohlke/pythonlibs/#scipy>`_, `Matplotlib
   <http://www.lfd.uci.edu/~gohlke/pythonlibs/#matplotlib>`_, `Nose
   <http://www.lfd.uci.edu/~gohlke/pythonlibs/#nose>`_, `Six
   <http://www.lfd.uci.edu/~gohlke/pythonlibs/#six>`_, `Tinyarray
   <http://www.lfd.uci.edu/~gohlke/pythonlibs/#tinyarray>`_, and `Kwant
   <http://www.lfd.uci.edu/~gohlke/pythonlibs/#kwant>`_ itself.  Make sure to
   put the downloaded files into a directory without any other ``.whl`` files.

7. Open a command prompt with administrator rights, as described in “How do I
   run a command with elevated permissions” at the `Microsoft Windows website
   <http://windows.microsoft.com/en-us/windows/command-prompt-faq>`_.

   Go to the directory with the ``.whl`` files, e.g.::

       cd c:\Users\YOUR_USERNAME\Downloads

   To install all the ``.whl``-files in the current directory, execute ::

       python -c "import pip, glob; pip.main(['install', '--no-deps'] + glob.glob('*.whl'))"

   The above cryptic command is equivalent to ``pip install --no-deps
   *.whl``, i.e. it installs all the wheel files in the current directory
   using pip.  Because the Windows command interpreter does not support globs,
   we have to rely on the globbing as provided by Python itself.

Now you are done, you can ``import kwant`` from within Python scripts.

(Note that many other useful scientific packages are available in Gohlke’s
repository.  For example, you might want to install `IPython
<http://www.lfd.uci.edu/~gohlke/pythonlibs/#ipython>`_ and its various
dependencies so that you can use the `IPython notebook <http://ipython.org/notebook.html>`_.)


Automatic installation using ``pip``
====================================

The most recent stable version of Kwant can be downloaded and installed
directly from the `Python package index <https://pypi.python.org/>`_ using
Python’s “pip“ tool::

    sudo pip3 install kwant

Pip can be also used to directly install the most recent development version
of Kwant directly from the Git repository::

    sudo pip3 install git+https://gitlab.kwant-project.org/kwant/kwant.git

Each of the above commands will perform a system-wide install (to
``/usr/local`` on Unix).  Type ``pip3 help install`` for installation options
and see `pip documentation <https://pip.readthedocs.org/>`_ for a detailed
description of ``pip``.

Note that ``pip`` will not install any non-Python dependencies such as `MUMPS
<http://graal.ens-lyon.fr/MUMPS/>`_.  These need to be installed in some other
way.  As an example, on a Debian or Ubuntu system, the following command will
install the non-Python prerequisites of Kwant::

    sudo apt-get install gfortran libopenblas-dev liblapack-dev libmumps-scotch-dev

The Kwant build scripts should find libraries that have been installed in the
above way automatically.  This will be signaled at the end of the build
process as follows::

    ******************************** Build summary ********************************
    Default LAPACK and BLAS
    Auto-configured MUMPS
    *******************************************************************************

On other platforms MUMPS will not be linked against.  If prepared packages are
not an option, the build process must be `configured manually
<doc/1/pre/install.html#build-configuration>`_.
