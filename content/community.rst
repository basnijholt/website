The Kwant community
===================

The Kwant project is an international collaboration.  Everybody is welcome to
participate in the community by asking and replying to questions, reporting
bugs, providing suggestions, and `contributing to the code and documentation
</contribute.html>`_.

A list of `Kwant authors </authors>`_ is included in the documentation.
Please only contact the authors directly for matters that cannot be discussed
in public on one of the mailing lists.


General mailing list
--------------------

The `kwant-discuss
<https://mailman-mail5.webfaction.com/listinfo/kwant-discuss>`_ mailing list is
the main public communication platform for anything related to Kwant: questions,
bug reports, discussions, and announcements.  You can use it in various ways:

- .. raw:: html

    <form class="form-inline"  method="get" action="https://www.mail-archive.com/search">
    <div class="input-group col-md-8">
    <input type="text" class="form-control" placeholder="Search query" size=25 name="q">
    <input type="hidden" name="l" value="kwant-discuss@kwant-project.org">
    <span class="input-group-btn">
    <input class="btn btn-default" type="submit" value="Search kwant-discuss">
    </span>
    </div>
    </form>

- .. raw:: html

    <form class="form-inline" method=post action="https://mailman-mail5.webfaction.com/subscribe/kwant-discuss">
    <div class="input-group col-md-8">
    <input type="text" class="form-control" placeholder="Your email address" name="email" size=25 onblur="if (this.value == '') {this.value='Your email address'}" onfocus="if (this.value == 'Your email address') {this.value=''}" />
    <span class="input-group-btn">
    <input class="btn btn-default" type="submit" name="email-button" value="Subscribe to kwant-discuss" />
    </span>
    </div>
    </form>

  … and receive new messages with regular email.  (To unsubscribe, use the `kwant-discuss options page <https://mailman-mail5.webfaction.com/options/kwant-discuss>`_.)

- You may follow the list through a web interface that also provides a RSS
  feed: `kwant-discuss on Mail-archive
  <https://www.mail-archive.com/kwant-discuss@kwant-project.org/>`_.

- Whether you are subscribed or not, you may post on the list simply by sending
  email to kwant-discuss@kwant-project.org.

- Please do not hesitate to join the discussion!  It’s easy to reply to a
  posting even if you are not subscribed (or receiving digests): (1) open the
  article in the web interface, (2) click on the button “Reply via email to” at
  the bottom of the page, (3) add the above list address under "CC".


Development list
----------------

Those who are interested the development of Kwant are invited to subscribe to
the `kwant-devel <https://mailman-mail5.webfaction.com/listinfo/kwant-devel>`_
mailing list.  This is the place for technical discussions about changes to
Kwant.  Please do not send bug reports to this list but rather to kwant-discuss.

Kwant-devel works in the same way as kwant-discuss:

- .. raw:: html

    <form class="form-inline"  method="get" action="https://www.mail-archive.com/search">
    <div class="input-group col-md-8">
    <input type="text" class="form-control" placeholder="Search query" size=25 name="q">
    <input type="hidden" name="l" value="kwant-devel@kwant-project.org">
    <span class="input-group-btn">
    <input class="btn btn-default" type="submit" value="Search kwant-devel">
    </span>
    </div>
    </form>

- .. raw:: html

    <form class="form-inline" method=post action="https://mailman-mail5.webfaction.com/subscribe/kwant-devel">
    <div class="input-group col-md-8">
    <input type="text" class="form-control" placeholder="Your email address" name="email" size=25 onblur="if (this.value == '') {this.value='Your email address'}" onfocus="if (this.value == 'Your email address') {this.value=''}" />
    <span class="input-group-btn">
    <input class="btn btn-default" type="submit" name="email-button" value="Subscribe to kwant-devel" />
    </span>
    </div>
    </form>

  (To unsubscribe, use the `kwant-devel options page <https://mailman-mail5.webfaction.com/options/kwant-devel>`_.)

- You may follow the list through a web interface that also provides a RSS
  feed: `kwant-devel on Mail-archive
  <https://www.mail-archive.com/kwant-devel@kwant-project.org/>`_.

- Whether you are subscribed or not, you may post on the list simply by sending
  email to kwant-devel@kwant-project.org.

- It’s easy to reply to a posting even if you are not subscribed (or receiving
  digests): (1) open the article in the web interface, (2) click on the button
  “Reply via email to” at the bottom of the page, (3) add the above list address
  under "CC".


Announcements (low-volume)
--------------------------

This read-only list is reserved for important announcements like new releases of
Kwant.  Only a few messages will be sent per year.  These announcements will be also posted on the main mailing list, so there is no need to subscribe to both lists.  We recommend every user of Kwant to subscribe at least to this list in order to stay informed about new developments.

- View archives: `kwant-announce on Mail-archive <https://www.mail-archive.com/kwant-announce@kwant-project.org/>`_.  Mail-archive provides RSS feeds.

- .. raw:: html

    <form method=post class="form-inline" action="https://mailman-mail5.webfaction.com/subscribe/kwant-announce">
    <div class="input-group col-md-8">
    <input type="text" class="form-control" placeholder="Your email address" name="email" size=25 onblur="if (this.value == '') {this.value='Your email address'}" onfocus="if (this.value == 'Your email address') {this.value=''}" />
    <span class="input-group-btn">
    <input class="btn btn-default" type="submit" name="email-button" value="Subscribe to the announcements" />
    </span>
    </div>
    </form>

  (To unsubscribe, use the `kwant-announce options page <https://mailman-mail5.webfaction.com/options/kwant-announce>`_.)


Reporting bugs
--------------

If you encounter a problem with Kwant, first try to reproduce it with as
simple a system as possible.  Double-check with the documentation that what
you observe is actually a bug in Kwant. If you think it is, please check `the
list of known bugs in Kwant
<https://gitlab.kwant-project.org/kwant/kwant/issues?label_name=bug>`_.  It
may be also a good idea to search or ask on the general mailing list. (You can
use the search box at the top of this page.)

If after checking you still think that you have found a bug, please add it to
the above-mentioned list of bugs by creating an issue with the “bug” label.  A
useful bug report should contain:

- The versions of software you are using: Kwant, Python, operating system, etc.

- A description of the problem, i.e. what exactly goes wrong.  This should
  include any relevant error messages.

- Enough information to reproduce the bug, preferably in the form of a simple
  script.
