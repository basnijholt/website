{#  -*- coding: utf-8 -*- #}
{% import 'base_helper.tmpl' as base with context %}
{% import 'annotation_helper.tmpl' as notes with context %}
{{ set_locale(lang) }}
{{ base.html_headstart() }}
{% block extra_head %}
{#  Leave this block alone. #}
{% endblock %}
{{ template_hooks['extra_head']() }}
</head>
<body>
<a href="#content" class="sr-only sr-only-focusable">{{ messages("Skip to main content") }}</a>

<!-- Menubar -->
<nav class="navbar navbar-default">
    <div class="container">
        <a class="navbar-brand" href="{{ abs_link(_link("root", None, lang)) }}">
        {% if logo_url %}
            <img src="{{ logo_url }}" alt="{{ blog_title|e }}" id="logo" class="img-responsive center-block">
        {% endif %}
        {% if show_blog_title %}
            <span id="blog-title">{{ blog_title|e }}</span>
        {% endif %}
        </a>

         <div class="havbar-header text-center" id="bs-navbar" aria-expanded="false">
            <ul class="nav navbar-nav well well-sm">
                {{ base.html_navigation_links() }}
                {{ template_hooks['menu']() }}
            </ul>
            {% if search_form %}
                {{ search_form }}
            {% endif %}

            <ul class="nav navbar-nav navbar-right">
                {% block belowtitle %}
                {% if translations|length > 1 %}
                    <li>{{ base.html_translations() }}</li>
                {% endif %}
                {% endblock %}
                {% if show_sourcelink %}
                    {% block sourcelink %}{% endblock %}
                {% endif %}
                {{ template_hooks['menu_alt']() }}
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container -->
</nav>
<!-- End of Menubar -->

<div class="container" id="content" role="main">
    <div class="body-content">
        <!--Body content-->
        <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            {{ template_hooks['page_header']() }}
            {% block content %}{% endblock %}
        </div>
        </div>
        <!--End of body content-->

        <footer id="footer" class="row">
            {{ content_footer }}
            {{ template_hooks['page_footer']() }}
        </footer>
    </div>
</div>

{{ base.late_load_js() }}
    <script>$('a.image-reference:not(.islink) img:not(.islink)').parent().colorbox({rel:"gal",maxWidth:"100%",maxHeight:"100%",scalePhotos:true});</script>
    <!-- fancy dates -->
    <script>
    moment.locale("{{ momentjs_locales[lang] }}");
    fancydates({{ date_fanciness }}, {{ js_date_format }});
    </script>
    <!-- end fancy dates -->
    {% block extra_js %}{% endblock %}
    {% if annotations and post and not post.meta('noannotations') %}
        {{ notes.code() }}
    {% elif not annotations and post and post.meta('annotations') %}
        {{ notes.code() }}
    {% endif %}
{{ body_end }}
{{ template_hooks['body_end']() }}
</body>
</html>
